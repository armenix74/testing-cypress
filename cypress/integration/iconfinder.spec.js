/// <reference types="Cypress" />

describe('My First Visit Test', () => {
    it('Visits the Kitchen Sink', () => {
        cy.visit('https://example.cypress.io')
        cy.contains("type").click()
        cy.url().should('include', '/commands/actions')
        // Get an input, type into it and verify that the value has been updated
        cy.get('.action-email')
            .type('fake@email.com')
            .should('have.value', 'fake@email.com')

    })
})

describe('Iconfinder Visit Test', () => {
    it('Visits Iconfinder', () => {
        cy.visit('https://www.iconfinder.com/')
        cy.contains("Micro")
        cy.contains("Starter")
        cy.contains("Unlimited")
    })
    it('Search icons', () => {
        cy.visit('https://www.iconfinder.com/')
        cy.get('.search-form .form-control').first()
            .type('email', { force: true })
            .should('have.value', 'email')
        cy.get('.search-form').first().submit({ force: true })
        cy.url().should('include', 'q=email')


    })


})

